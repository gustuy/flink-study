建议用IDEA打开工程
代码文件在FlinkFinalJob->src->main->Scala->Course->work->Data.scala文件

数据可视化在FlinkFinalJob->src->main->HTML->Echart.html

数据集在FlinkFinalJob->src->main->resources中，代码执行的逻辑过程图和可视化演示也在resources中

如果Data.scala无法正常运行，检查是否将Scala文件夹标记为Sources Root文件
标记方法：右键Scala文件夹->Mark Directory as->Sources Root